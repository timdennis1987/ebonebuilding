<div class="container d-block d-md-none">
    <div class="row">
        <p class="text-center mb-1">
            <a href="/">
                <img src="public/images/logo.png" width="100%" alt="">
            </a>
        </p>
    </div>
</div>

<section id="contact-bar" class="text-white">
    <div class="container d-none d-md-block">
        <div class="row">
            <div class="col-6">
                <i class="fas fa-phone mr-2"></i>
                <a href="">
                    09876543210
                </a>
                <i class="fas fa-envelope mr-2 ml-3"></i>
                <a href="">
                    test@test.com
                </a>
            </div>
            <div class="col-6 text-right">
                <a href="">
                    <i class="fab fa-facebook fa-2x"></i>
                </a>
                <a href="">
                    <i class="fab fa-instagram fa-2x ml-3"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="d-block d-md-none">
        <div class="row mx-0">
            <div class="col-6 pl-0 border-right text-center">
                <a class="nav-link" href="">
                    <i class="fas fa-phone"></i>
                </a>
            </div>
            <div class="col pl-0 text-center">
                <a class="nav-link" href="">
                    <i class="fas fa-envelope"></i>
                </a>
            </div>
        </div>
    </div>
</section>

<nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom border-top shadow">

    <div class="container px-0">
        <a class="navbar-brand d-block d-md-none" href="/">
            {{ str_replace('_', ' ', config('app.name')) }}
        </a>
        <a class="nav-logo navbar-brand" href="/">
            <img src="public/images/logo.png" width="50%" alt="">
        </a>

        <button class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation">
            MENU
        </button>

        <div class="collapse navbar-collapse font-weight-bold" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/about-us">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/gallery">Projects</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact">Contact</a>
                </li>
                <li class="nav-item pt-1 mr-2 d-block d-md-none">
                    <a class=""
                       href=""
                       target="_blank">
                        <i class="fab fa-facebook-square fa-2x mr-2"></i>
                    </a>
                    <a class=""
                       href=""
                       target="_blank">
                        <i class="fab fa-instagram fa-2x"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
