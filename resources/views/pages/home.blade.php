@extends('templates.main')
@include('includes.header')
@section('content')

    <section id="hero"
             style="background: -webkit-gradient(linear, left top, left bottom, from(rgba(103, 103, 103, 0)), to(#fff)), url(http://develop.ebonebuilding.co.uk/public/images/hero-white.jpg) top right;"
             class="">
        <div class="container text-dark border-left pl-3">

            <h2 class="py-3">KING'S LYNN BUILDERS</h2>
            <h1 class="gutter-right">YOUR TRUSTED LOCAL BUILDER IN KING'S LYNN.</h1>
            <h3 class="mt-3">BRICKWORK | STONEWORK | PATIOS | EXTENSIONS | GENERAL BUILDING</h3>

            <div class="mt-5 pb-3">
                <a href=""
                   class="btn btn-primary btn-lg">
                    09876543210
                </a>
            </div>
        </div>
    </section>

@endsection
