<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WpSite extends Model
{
    public function __construct()
    {
        $this->wp_url = 'https://support.ebonebuilding.co.uk/wp-json/wp/v2/';
    }

    protected function getJson($url)
    {
        $response = file_get_contents($url, false);
        return json_decode($response);
    }

    public function wpPageData($slug)
    {
        $pageData = $this->getJson($this->wp_url . 'pages?slug=' . $slug);
        $pageData = $pageData[0];

        return [
            'id'      => $pageData->id,
            'meta'    => $pageData->yoast_head,
            'title'   => $pageData->title->rendered,
            'content' => $pageData->content->rendered,
//            'h1'      => $pageData->acf->hero_h1,
//            'h2'      => $pageData->acf->hero_h2
        ];
    }
}
