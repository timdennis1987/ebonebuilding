<?php

namespace App\Http\Controllers;

use App\Models\WpSite;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function wp()
    {
        return new WpSite;
    }

    public function homeView()
    {
        $data = $this->wp()->wpPageData('home');

        return view('pages.home', [
            'data' => $data,
        ]);
    }
}
